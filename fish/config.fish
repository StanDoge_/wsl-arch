#PERSONAL CONFIGS
set fish_greeting ""

#Vi-mode
fish_vi_key_bindings
set -gx EDITOR nvim #nvim > vim

#Aliases
alias hack "nvim ~/.config/fish/config.fish"
alias fresh "source ~/.config/fish/config.fish"
alias q "cd ; exit"
alias l "exa --long --header --git --group-directories-first"
alias lt "exa --header --git --group-directories-first --tree --level=2 -a --icons"
alias dpress "~/development/Bash/file-decompressor/dpress.sh"
alias f "explorer.exe ."
alias py "python"
alias drive "cd /mnt/g/My\ Drive/Universidad/ ; ranger"
alias hub "cd /mnt/c/Users/kevin/Downloads/"
alias pn 'pnpm'
alias dk 'docker'
alias dc 'docker compose'
alias rvim 'sudo rm -rf ~/.config/nvim && rm -rf ~/.local/share/nvim && rm -rf ~/.cache/nvim'

#OhMyZsh aliases
alias g 'git'
alias gaa 'git add --all'
alias gc 'git clone'
alias gcam 'git commit -a -m'
alias gpush 'git push origin $(current_branch)'
alias gpull 'git pull origin $(current_branch)'
alias gswc 'git switch -c'
alias gswm 'git switch main'
alias gswd 'git switch development'
alias gsw 'git switch'
alias gcmd 'git commit --amend -m'
alias gcams 'git commit -S -am'
alias gd 'git diff'
alias gdt 'git diff-tree --no-commit-id --name-only -r'
alias gdw 'git diff --word-diff'
alias gf 'git fetch'
alias lg 'lazygit'
alias glgp 'git log --stat -p'
alias glgg 'git log --graph'
alias glgga 'git log --graph --decorate --all'
alias gm 'git merge'
alias gcach 'git rm -rf --cached .'

alias gra 'git remote add'
alias grrm 'git remote remove'
alias grv 'git remote -v'

alias gsb 'git status -sb'
alias gsps 'git show --pretty short --show-signature'
alias gss 'git status -s'

#Funtions
function mk -d "Create a directory and set CWD"
    command mkdir $argv
    if test $status = 0
        switch $argv[(count $argv)]
            case '-*'

            case '*'
                cd $argv[(count $argv)]
                return
        end
    end
end

function dev -d "Move towards development folder with ranger or not"
  set number $(count $argv)
  if test $number -eq 0
   cd ~/development ; ranger .
  else 
    cd ~/development/$argv[1]
  end
end

function current_branch -d "Returns HEAD in current dir"
    git rev-parse --abbrev-ref HEAD
end

function fish_mode_prompt -d "Removes annoying vi propm"; end

set -gx VOLTA_HOME "$HOME/.volta"
set -gx PATH "$VOLTA_HOME/bin" $PATH

# pnpm
set -gx PNPM_HOME "/home/doge/.local/share/pnpm"
set -gx PATH "$PNPM_HOME" $PATH
# pnpm end
